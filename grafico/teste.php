<?php include("../Excel/generate_excel.php")?>
<!DOCTYPE html>
<html>
  <head>
    <title>Creating Dynamic Data Graph using PHP and Chart.js</title>
    <style type="text/css">
      BODY {
        width: 550px;
      }

      #chart-container {
        width: 100%;
        height: auto;
      }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  </head>
  <body>
    <div id="chart-container">
      <canvas id="graphCanvas"></canvas>
    </div>

    <script>
      $(document).ready(function () {
        showGraph();
      });

      function showGraph() {
        {
          var name = [<?php foreach ($tasks as $row){echo "\"".$row["Nome"]."\",";}?>];
          var marks = [<?php foreach ($tasks as $row){echo "\"".$row["Contacto"]."\",";}?>];

          var chartdata = {
            labels: name,
            datasets: [
              {
                label: "Vendas de Fevereiro",
                backgroundColor: "#0000ff",
                borderColor: "#00ff00",
                hoverBackgroundColor: "#00ccff",
                hoverBorderColor: "#666666",
                data: marks,
              },
            ],
          };

          var graphTarget = $("#graphCanvas");

          var barGraph = new Chart(graphTarget, {
            type: "bar",
            data: chartdata,
          });
        }
      }
    </script>
  </body>
</html>
