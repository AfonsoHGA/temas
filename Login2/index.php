<?php

function login(){
    $con = $this->getPDO();
    if(isset($_POST["nome"])){
        $user = $_POST["nome"];
        $pass = $_POST["pass"];
        $sql = "SELECT * FROM users WHERE nome_u=?";
        try{
            $sql2=$con->prepare($sql);
            $sql2->execute([$user]);
            $results=$sql2->fetchAll();
        }catch(PDOException $e){
            echo "O erro é " . $e->getMessage(); 
        }
        if(count($results)>0){
            foreach($results as $r){
                $block_u=$r["block_u"];
            }
            if($block_u==1){
                return "user_block";
            }
            $cipher = "aes-256-cbc"; 
            $iv = "32cru3cr32ucrm02"; 
            $encrypted_data = openssl_encrypt($pass, $cipher, $pass, 0, $iv); 
            //$pass_r = openssl_decrypt($pass_u, $cipher, $pass_u, 0, $iv);
            if($r["pass_u"]== $encrypted_data){
                $sql="UPDATE users SET falhas_u=0 WHERE id_u={$r["id_u"]}";
                $con->query($sql);
                $_SESSION["user"] = $user;
                $_SESSION["nivel"] = $r["nivel_u"];
                header("location:teste.php");
            }else{
                $falhas=$r["falhas_u"]+1;
                if($falhas<3){
                    $sql="UPDATE users SET falhas_u=$falhas WHERE id_u={$r["id_u"]}";
                }else{
                    $sql="UPDATE users SET falhas_u=$falhas, block_u=1 WHERE id_u={$r["id_u"]}";
                }
                $con->query($sql);
                return "pass_errada";
            }
        }else{
            return "user_errado";
        }
    }
}