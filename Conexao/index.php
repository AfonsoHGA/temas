<?php
function pre($a, $die=0){
    echo "<pre>";
    print_r($a);
    echo "</pre>";
    if($die==1){
        die();
    }
}
class BD{
    protected  $pdo;
    protected $opcoes = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false
    ];
    function __construct(){
        //require_once("../../htconfigs/server.php");
        $server="localhost";
        $bd="teste";
        $user="root";
        $pass="";
        $dsn="mysql:host=$server;dbname=$bd;charset=utf8mb4";
        try{
            $this->pdo=new PDO($dsn, $user, $pass, $this->opcoes);
        }catch(PDOException $erro){
            echo "O erro foi: ".$erro->getMessage();
        }
    }
    public function getPDO(){
        return $this->pdo;
    }
}