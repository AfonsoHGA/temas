function myScrollFunc () {
    var myID = document.getElementById("myID");
    var y = window.scrollY;
    if (y >= 200) {
        myID.classList.add("opacity-100");
    } else {
        myID.classList.remove("opacity-100");
    }
};
window.addEventListener("scroll", myScrollFunc);

function abrirMenu(){
    var Menu = document.getElementById("MenuGrande");
    Menu.classList.remove("hidden");

}
function fecharMenu(){
    var Menu = document.getElementById("MenuGrande");
    Menu.classList.add("hidden");

}