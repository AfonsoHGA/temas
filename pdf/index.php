<?php
session_start();
isset($_SESSION["user"])?"":header("location:home.php");
require("fpdf184/fpdf.php");
class Bd
{
    protected $conexao;
    protected $opcoes = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false
    ];

    public function __construct(){
        $host="localhost";
        $bd="imo";
        $user="root";
        $pass="";

        $dsn="mysql:host=$host;dbname=$bd;charset=utf8mb4";
        $opcoes=$this->opcoes;  
        try{
            $con=new PDO($dsn,$user,$pass,$opcoes);
            $this->conexao= $con;
        }catch(PDOException $e ){
            die("Morre Mãe da Foca: ".$e->getMessage());
        }
    }
    
    public function getPDO(){
        return $this->conexao;
    }
}
function convert_txt($txt){
    return utf8_decode($txt);
}
function formataEuro($n){
    $nf = number_format($n,0,"."," ")." euros";
    return $nf;
}
$teste = new Bd();
$conexao = $teste->getPdo();

$sql = "SELECT * FROM vendas LEFT JOIN imoveis ON imovel_v=id_i LEFT JOIN consultores ON consultor_v=id_co LEFT JOIN clientes ON cliente_v=id_cl LEFT JOIN tipos ON tipo_i=id_t LEFT JOIN localidades ON local_i=id_l LEFT JOIN distritos AS dco ON distrito_l=dco.id_d LEFT JOIN sedes ON sede_co=id_s LEFT JOIN distritos AS ds ON distrito_l=ds.id_d WHERE id_v={$_GET['venda']}";
$resposta2= $conexao->query($sql);
$r = $resposta2->fetch();
class MeuPDF extends FPDF{
    
    function Header(){
        // Logo
        //$this->Image('logo.png',10,6,30);
        $this->SetFont('Arial','B',25);
        $this->SetFillColor(0,128,0);
        $this->SetTextColor(255,255,255);
        $this->Cell(210,15,convert_txt('ImoSclalabis'), 0, 1, "C", 1);
    }
    function Footer(){
        $this->sety(-10);
        $this->SetMargins(0,0,0);
        $this->Cell(190,0,'',0,1);
        // Logo
        //$this->Image('logo.png',10,6,30);
        $this->SetFont('Arial','B',15);
        $this->SetFillColor(0,128,0);
        $this->SettextColor(0,128,0);
        $this->Cell(210,10,convert_txt("ImoSclalabis"),0,1,"C",1);
        }
}
$pdf = new MeuPDF();
    $pdf->SetMargins(0,0,0);
    $pdf->AddPage(); //AddPage("P", "A4", "360");

    $pdf->SetMargins(10,10,0);
    $pdf->Cell(190,7,'',0,1);
    $pdf->SetFont('Arial','B',25);
    $pdf->Cell(190,10,convert_txt('Contrato de Venda'),0,1, "C");

    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetMargins(10,10,0);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,10,'',0,1);
    $pdf->Multicell(190,7,convert_txt("ImoSclalabis, {$r['nome_d']}"), 0, 0, "L");
    $pdf->SetFont('Arial','',11);
    $pdf->Multicell(190,7,convert_txt("{$r['morada_s']}"), 0, 0, "L");
    $pdf->Multicell(190,7,convert_txt("+351 {$r['tele_s']}"), 0, 0, "L");

    $pdf->SetMargins(105,10,0);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,10,'',0,1);
    $pdf->Multicell(95,7,convert_txt("Cliente"), 0, 0, "L");
    $pdf->SetFont('Arial','',11);
    $pdf->Multicell(95,7,convert_txt("{$r['nome_cl']}"), 0, 0, "L");
    $pdf->Multicell(95,7,convert_txt("Email: {$r['email_cl']}"), 0, 0, "L");
    $pdf->Multicell(95,7,convert_txt("Tel: {$r['contacto_cl']}"), 0, 0, "L");
    $pdf->Multicell(95,7,convert_txt("Morada: {$r['morada_cl']} - {$r['cod_post_cl']}"), 0, 0, "L");

    $pdf->SetMargins(10,10,0);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(190,10,'',0,1);
    $pdf->Multicell(190,7,convert_txt("Descrição"), 0, 0, "L");
    $pdf->SetFont('Arial','',11);
    $nasc_cl=date("d/m/Y",strtotime($r["nasc_cl"]));
    $preco_iva_i=formataEuro($r['preco_i']*1.23);
    $c=($r['preco_i']>300000)?0.03:0.02; $preco_com_i=formataEuro($r['preco_i']*$c);
    $pdf->Multicell(190,7,convert_txt("\n   O cliente {$r['nome_cl']} nascido a {$nasc_cl} com o NIF {$r['nif_cl']} está a comprar um imovel, {$r['nome_t']} com {$r['quartos_i']} quartos e {$r['wcs_i']} wcs localizado na {$r['morada_i']}, \"{$r['estado_i']}\".\n  O Imovel está a ser vendido a $preco_iva_i pelo vendedor {$r['nome_co']} que fica com $preco_com_i de comissão."), 0, "J");

    $pdf->SetMargins(15,10,0);
    $pdf->Cell(100,10,'',0,1);
    $pdf->Image( "imagens/imoveis/{$r['imagem_i']}",null, null, 60, 0);

    $pdf->SetMargins(105,10,0);
    $pdf->Cell(50,10,'',0,1);
    $pdf->SetFont('Arial','B',11);
    $pdf->MultiCell(90,7,"Vendedor",0,"C");
    $pdf->MultiCell(90,7,"_________________________________",0,"C");
    $pdf->Cell(50,10,'',0,1);
    $pdf->MultiCell(90,7,"Cliente",0,"C");
    $pdf->MultiCell(90,7,"_________________________________",0,"C");

    

$pdf->Output();
?>