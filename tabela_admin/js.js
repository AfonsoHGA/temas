$(document).ready(function() {
    var table = $('#tabela').DataTable( {
        rowReorder: true,
        ordering: true,
        "order": [[0, 'asc']],
        /* info: false,
        responsive: true,
        paging: false,
        searching: false, */
        language: {
            "info": "A ver de _START_ a _END_ (total _TOTAL_)",
            "zeroRecords": "Nenhum registo encontrado",
            "infoEmpty": "Nenhum registo encontrado",
            "infoFiltered": "(filtrado de _MAX_ registos)"
        },
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 0 },
            { orderable: true, className: 'reorder', targets: 2 },
            { orderable: false, targets: '_all' },
            { visible: false, targets: 1 }
        ]
    } );
    table.on('row-reorder', function (dragEvent, data, nodes) {
        for (var i = 0, ien = data.length ; i < ien ; i++) {
            var rowData = table.row(data[i].node).data();
            $.ajax({
                type: "GET",
                cache: false,
                contentType: "application/json; charset=utf-8",
                url: 'categorias.php',
                data: { Id: rowData[1], fromPosition: data[i].oldData, toPosition: data[i].newData },
                dataType: "json"
            });
            var logs = { Id: rowData[1], fromPosition: data[i].oldData, toPosition: data[i].newData }
            //console.log(logs)
        }
    });
} );