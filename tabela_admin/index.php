<div class="container">
        <div class="row">
            <div class="flex h-10 items-center">
                <a href="categorias.php?ad" class="border bg-gray-100 col-lg-3 text-xl font-medium rounded text-center">Adicionar</a>
            </div>
            <table class="table table-striped table-bordered w-100" id="tabela">
                <thead>
                    <tr>
                        <th class="col-1">Ordem</th>
                        <th class="col-1">Id</th>
                        <th>Nome</th>
                        <th class="col-1"></th>
                        <th class="col-1">Gerir</th>
                        <th class="col-1">Editar</th>
                        <th class="col-1">Eliminar</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        $cats=$gp->categorias("lista");
                        foreach($cats as $c):
                    ?>
                    <tr>
                        <td class="col-1 py-4"><?=$c["ordem_c"]?></td>
                        <td class="col-1"><?=$c["id_c"]?></td>
                        <td><div class="flex py-3"><?=$c["nome_pt_c"]?></div></td>
                        <td><img src="imgs/<?=$c["imagem_c"]?>" class="img-fluid"></td>
                        <td class="col-1"><a href="produtos.php?cat=<?=$c["id_c"]?>"><div class="flex justify-center py-3"><img src="icones/list-solid.svg" width="30px"></div></a></td>
                        <td class="col-1"><a href="categorias.php?up=<?=$c["id_c"]?>"><div class="flex justify-center py-3"><img src="icones/pen-to-square-solid.svg" width="30px"></div></a></td>
                        <td class="col-1"><a href="categorias.php?del=<?=$c["id_c"]?>"><div class="flex justify-center py-3"><img src="icones/trash-can-solid.svg" width="30px"></div></a></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js" integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.13.2/js/dataTables.bootstrap.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/rowreorder/1.3.2/js/dataTables.rowReorder.min.js"></script> -->


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/ju/dt-1.13.2/rr-1.3.2/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/ju/dt-1.13.2/rr-1.3.2/datatables.min.js"></script>

<script src="uteis.js"></script>